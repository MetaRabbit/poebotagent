﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;

namespace PoeBotAgent.Utils.System
{
    public class WindowsControl
    {
        [DllImport("user32.dll")]
        public static extern IntPtr FindWindow(String sClassName, String sAppName);

        public static bool IsExistsWindows(List<string> names)
        {

            if (names == null)
            {
                return false;
            }

            foreach (IntPtr hwnd in names.Select(windowName => FindWindow(null, windowName)))
            {
                if (hwnd.ToInt32() != 0)
                {
                    return true;
                }
                Console.WriteLine(hwnd.ToInt32());
            }

            return false;
        }
    }
}
