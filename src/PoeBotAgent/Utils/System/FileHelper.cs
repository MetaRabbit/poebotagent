﻿using System.IO;

namespace PoeBotAgent.Utils.System
{
    public class FileHelper
    {
        public static void CopyDir(string fromDir, string toDir)
        {
            if (!Directory.Exists(toDir))
            {
                Directory.CreateDirectory(toDir);
            }
            foreach (string s1 in Directory.GetFiles(fromDir))
            {
                string s2 = toDir + "\\" + Path.GetFileName(s1);
                File.Copy(s1, s2, true);
            }
            foreach (string s in Directory.GetDirectories(fromDir))
            {
                CopyDir(s, toDir + "\\" + Path.GetFileName(s));
            }
        }
    }
}
