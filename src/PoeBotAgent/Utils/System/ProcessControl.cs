﻿using System;
using System.Diagnostics;
using System.IO;
using PoeBotAgent.Enum;

namespace PoeBotAgent.Utils.System
{
    public class ProcessControl
    {
        public static int SleepTime = 60000;

        public ProcessControl()
        {

        }

        public static bool IsExecute(string nameProc)
        {
            //TODO Тчательную проверку процесса по прошлому имени из FileInfo
            var processes = Process.GetProcessesByName(nameProc);
            if (processes.Length == 0)
            {
                return false;
            }

            return true;
        }


        public static void Start(string dir, string nameExe)
        {
            if (IsExecute(nameExe))
            {
                return;

            }
            //Запускаем 
            try
            {
                var startInfo = new ProcessStartInfo
                {
                    WorkingDirectory = dir,
                    FileName = Path.Combine(dir, nameExe),
                    UseShellExecute = false
                };
                Process.Start(startInfo);
            }
            catch (Exception)
            {
                //Пишем в лог ошибку запуска

            }
        }

        public static void Restart(string dir, string nameExe)
        {
            if (!IsExecute(nameExe))
            {
                return;
                
            }           
            Kill(nameExe);
            Start(dir, nameExe);
        }

    

        public static void ForceRestart(string dir, string nameExe)
        {
            Kill(nameExe);
            Start(dir, nameExe);
        }

        public static void Kill(string nameProcess)
        {
            var processes = Process.GetProcessesByName(nameProcess);
            foreach (var process in processes)
            {
                process.Kill();
            }

        }
        /// <summary>
        /// Переключения состояния процесса
        /// </summary>
        /// <param name="type"></param>
        /// <param name="dir"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static bool SwitchRunning(Running type, string dir, string name)
        {
            if (string.IsNullOrEmpty(dir) || string.IsNullOrEmpty(name))
            {
                //Выдаем предупреждение, что запуск невозможен
                Console.WriteLine();
                return false;
            }
            switch (type)
            {
                case Running.Disable:
                    {
                        Kill(name);
                        return false;
                    }
                case Running.Enable:
                    {
                        Start(dir, name);
                        return true;
                    
                    }
                case Running.Reload:
                    {
                        Restart(dir, name);
                        return true;
                  
                    }
            }
            return false;
        }
    }
}
