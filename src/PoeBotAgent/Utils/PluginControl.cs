﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json.Linq;
using PoeBotAgent.Core;
using PoeBotAgent.Service;
using Serilog;

namespace PoeBotAgent.Utils
{
    public class PluginControl
    {
        Configuration _configuration;

        public PluginControl(Configuration configuration)
        {
            _configuration = configuration;
        }

        public string GetSettingPluginByName(string name)
        {
            if (string.IsNullOrEmpty(_configuration.Settings.AbsPathBotDirectory) || string.IsNullOrEmpty(_configuration.Settings.PathToSettingsPlugins))
            {
                Log.Error("Невозможно выполнить взятие настроек. Не задан один из двух, либо оба пути в глобальных настройках: AbsPathBotDirectory и PathToSettingsPlugins.");
                return "Невозможно выполнить взятие настроек. Не задан один из двух, либо оба пути в глобальных настройках: AbsPathBotDirectory и PathToSettingsPlugins.";
            }

            try
            {
                if (!File.Exists(Path.Combine(_configuration.Settings.AbsPathBotDirectory,
                  _configuration.Settings.PathToSettingsPlugins, name + ".json")))
                {
                    Log.Error("Файл настроек плагина не найден");
                    return "Файл настроек плагина не найден";
                }

                return File.ReadAllText(Path.Combine(_configuration.Settings.AbsPathBotDirectory,
                    _configuration.Settings.PathToSettingsPlugins, name + ".json"));
            }
            catch (Exception)
            {
                Log.Error("Неправильный формат следующих путей: AbsPathBotDirectory и PathToSettingsPlugins");
                return "Неправильный формат следующих путей: AbsPathBotDirectory и PathToSettingsPlugins";
            }
          
        }

        public void ChangeSettings(string name, List<string> items)
        {
            JObject settings = JObject.Parse(GetSettingPluginByName(name));

            foreach (var item in items)
            {

                var key = item.Split(':')[0];
                var value = item.Split(':')[1];
                JToken token;
                if (settings.TryGetValue(key, out token))
                {
                    int result;
                    if (Int32.TryParse(value, out result))
                    {
                        settings[key] = result;
                        continue;
                    }

                    bool bresult;
                    if (bool.TryParse(value, out bresult))
                    {
                        settings[key] = bresult;
                        continue;
                    }

                    settings[key] = value;
                }
            }
            //Сохраняем изменения
            File.WriteAllText(Path.Combine(_configuration.Settings.AbsPathBotDirectory,
                _configuration.Settings.PathToSettingsPlugins, name + ".json"), settings.ToString());

        }

        public IEnumerable<string> GetListPlugins()
        {

                if (string.IsNullOrEmpty(_configuration.Settings.AbsPathBotDirectory) || string.IsNullOrEmpty(_configuration.Settings.PathPlugins))
                {
                    Log.Error("Невозможно взять список плагинов из-за отсутствия одного или нескольких путей в глобальной файле настроек: AbsPathBotDirectory и PathPlugins");
                    return new List<string>() { "Невозможно взять список плагинов из-за отсутствия одного или нескольких путей в глобальной файле настроек: AbsPathBotDirectory и PathPlugins" };
                }

            try
            {
                var files = Directory.GetFileSystemEntries(Path.Combine(_configuration.Settings.AbsPathBotDirectory,
                   _configuration.Settings.PathPlugins));
                return files.Select(s => new FileInfo(s).Name);
            }
            catch (Exception)
            {
                Log.Error("Направильный формат для следующих путей: AbsPathBotDirectory и PathPlugins");
                return new List<string>() { "Направильный формат для следующих путей: AbsPathBotDirectory и PathPlugins" };
            }
               
           
        } 
    }
}
