﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PoeBotAgent.Models
{
    public class BotSettings
    {
        public BotSettings()
        {
            AdvanceItems = new List<string>();
        }

        //Основной бот
        public string MainBot { get; set; }
        //Основная рутина
        public string MainRoutine { get; set; }
        //Ключ бота
        public string Key { get; set; }
        //Дополнительные параметры для настроек
        public List<string> AdvanceItems { get; set; } 
    }
}
