﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PoeBotAgent.Enum;

namespace PoeBotAgent.Models
{
    public class Command
    {
        public TypeObject Object { get; set; }

        public List<string> Items { get; set; }
        
        public string Action { set; get; }
    }
}
