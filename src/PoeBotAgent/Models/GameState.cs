﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PoeBotAgent.Enum;

namespace PoeBotAgent.Models
{
    public class GameState
    {
        public Running? Running { get; set; }
        public List<string> Settings { get; set; }
        public bool Default { get; set; }
    }
}
