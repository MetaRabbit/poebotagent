﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PoeBotAgent.Enum;

namespace PoeBotAgent.Models
{
    public class Settings
    {
        //Настройки бота 
        //Название бота - название процесса
        public string BotName;
        //Тип поведения агента
        public TypeAgent Type;
        //Версия бота
        public string Version { get; set; }
        //страница с версией бота
        public string UrlUpdatePage { get; set; }
        //Префикс к ссылке скачивания
        public string PrefixDownloadLing { get; set; }
        //Абсолютный путь к содержимому папке бота
        public string AbsPathBotDirectory { get; set; }
        //Относительный путь к файлам лога
        public string PathLogs { get; set; }
        //Относительный путь к содержимому папке с плагинами
        public string PathPlugins { get; set; }
        //Имя исполняемого файла
        public string NameBotExeFile { get; set; }
        //Относительный путь к настройкам с названием файла
        public string PathBotSettingsFile { get; set; }
        //bot\Settings\Default
        public string PathToSettingsPlugins { get; set; }

        //Настройки игры
        //Названия игры
        public string NameGame { get; set; }
        //Абсолютный путь к содержимому папки игры
        public string AbsPathGameDirectory { get; set; }
        //Название исполняемого файла игры
        public string NameGameExeFile { get; set; }
        //Путь к настройкам с именем и разрешением
        public string AbsPathGameSettings { get; set; }
        //Названия окон обновлений.
        public List<string> UpdateWindowsNameList { get; set; }

    }
}
