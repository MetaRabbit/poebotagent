﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PoeBotAgent.Enum;

namespace PoeBotAgent.Models
{
    public class BotState
    {
        public Running? Running  { get; set; }
        public BotSettings BotSettings { get; set; }
        //Аргументы для старта
        public string StartArgs { get; set; }
        //Необходимость апдейта
        public bool IsNeedUpdate { get; set; }   
    }
}
