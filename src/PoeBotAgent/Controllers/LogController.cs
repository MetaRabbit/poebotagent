﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.AspNet.Mvc;
using Microsoft.Extensions.PlatformAbstractions;
using Serilog;

namespace PoeBotAgent.Controllers
{
    using static File;

    [Route("_lastlog")]
    public class LogController : Controller
    {
        IApplicationEnvironment _envApp;
        // GET: api/values
        public LogController(IApplicationEnvironment envApp)
        {
            _envApp = envApp;
        }

        [HttpGet]
        public string Get()
        {
            //Выбор файла с минимальной датой
            var lastDateLog = Directory.GetFiles(Path.Combine(_envApp.ApplicationBasePath, "Logs")).Select(s => s.Split('-')[1]).Select(s => s.Split('.')[0]).ToArray().Max();
            lastDateLog = Path.Combine(_envApp.ApplicationBasePath, @"Logs\log-" + lastDateLog + ".txt");
            //Удаляем последний лог, если существует
            if (Exists("Log"))
            {
                Delete("Log");
            }
            //Копируем в новый файл актульный лог, т.к. логи из папки Logs заняты логером
            Copy(lastDateLog, "Log");     
            //Выводим     
            return ReadAllText("Log");
        }
        //Положить настройки уровня логирования
        // PUT api/values/5
        [HttpPut]
        public void Put([FromBody]string level)
        {
       
        }
    }
}
