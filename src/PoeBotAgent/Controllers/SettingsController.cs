﻿using Microsoft.AspNet.Mvc;
using PoeBotAgent.Core;
using PoeBotAgent.Enum;
using PoeBotAgent.Extends;
using PoeBotAgent.Models;
using PoeBotAgent.Service;

namespace PoeBotAgent.Controllers
{
    [Route("[controller]")]
    public class SettingsController : Controller
    {
        readonly Configuration _botConfiguration;

        public SettingsController(Configuration botConfiguration)
        {
            _botConfiguration = botConfiguration;
        }

        // GET: api/values
        [HttpGet]
        public JsonResult Get()
        {
            return Json(_botConfiguration.Settings);
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]Settings data)
        {
            if (data == null)
            {
                Response.BadRequest();
            }

            _botConfiguration.AddOrUpdate(data);

        }

        /// <summary>
        /// Загрузить настройки бота
        /// </summary>
        /// <param name="data"></param>
        // PUT api/values/5
        [HttpPut]
        public void Put([FromBody]Settings data)
        {
            if (data == null)
            {
                Response.BadRequest();
            }

            _botConfiguration.AddOrUpdate(data);

        }
    }
}
