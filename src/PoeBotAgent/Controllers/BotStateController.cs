﻿using System.Collections.Generic;
using System.Text;
using Microsoft.AspNet.Mvc;
using PoeBotAgent.Core;
using PoeBotAgent.Enum;
using PoeBotAgent.Extends;
using PoeBotAgent.Models;
using PoeBotAgent.Service;

namespace PoeBotAgent.Controllers
{
    [Route("api/[controller]")]
    public class BotStateController : Controller
    {
        BotService _botService;
        BotConfiguration _botConfiguration;

        public BotStateController(BotService botService, BotConfiguration botConfiguration)
        {
            _botService = botService;
            _botConfiguration = botConfiguration;
        }

        // GET: api/values
        [HttpGet]
        public JsonResult Get()
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.AppendLine("Running: " + _botService.Running);
            stringBuilder.AppendLine("MainBot: " + _botService.BotConfiguration.BotSettingsChanger.MainBot);
            stringBuilder.AppendLine("MainRoutine: " + _botService.BotConfiguration.BotSettingsChanger.MainRoutine);
            stringBuilder.AppendLine("Key: " + _botService.BotConfiguration.BotSettingsChanger.AuthKeys);
            if (!string.IsNullOrEmpty(_botService.BotConfiguration.ArgsRun))
            {
                stringBuilder.AppendLine("StartArgs: " + _botService.BotConfiguration.ArgsRun);
            }
            
            string state = stringBuilder.ToString();
            return Json(new { State = state, _botConfiguration.AppBotSettings});
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut]
        public void Put([FromBody]BotState botState)
        {
            if (botState == null)
            {
                Response.BadRequest();
                return;
            }

            _botService.ChangeState(botState);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
