﻿using System;
using System.Collections.Generic;
using Microsoft.AspNet.Mvc;
using PoeBotAgent.Core;
using PoeBotAgent.Extends;
using PoeBotAgent.Models;
using PoeBotAgent.Service;

namespace PoeBotAgent.Controllers
{
    //Контроллер, отвечающий за состояние игры
    [Route("api/[controller]")]
    public class GameStateController : Controller
    {
        GameService _gameService;

        public GameStateController(GameService gameService)
        {
            _gameService = gameService;
        }

        /// <summary>
        /// Взятие состояния игры. Состоние + графические настройки
        /// </summary>
        /// <returns></returns>
        // GET: api/values
        [HttpGet]
        public JsonResult Get()
        {
            return Json(new
            {
                Running = _gameService.Running,
                Updating = _gameService.IsUpdating,
                Settings = _gameService.Settings
            });
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }
        //Изменение состояния игры посредством команды в id
        // POST api/values
        [HttpPost]
        public void Post([FromBody]GameState gameState)
        {
            try
            {
                _gameService.UseDefaultSettings(gameState.Default);
                _gameService.ChangeState(gameState.Settings);
                if (gameState.Running != null)
                {
                    _gameService.TrySwitchRunning(gameState.Running.Value);
                }

            }
            catch (Exception)
            {
                Response.BadRequest();
            }
           
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
