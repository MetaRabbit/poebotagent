﻿using System.Collections.Generic;
using Microsoft.AspNet.Mvc;

namespace PoeBotAgent.Controllers
{
    [Route("[controller]")]
    public class StatusController : Controller
    {
        // GET: status
        [HttpGet]
        public JsonResult Get()
        {


            return Json("Idle");
        }

    }
}
