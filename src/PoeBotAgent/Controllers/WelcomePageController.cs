﻿using Microsoft.AspNet.Mvc;

namespace PoeBotAgent.Controllers
{
    public class WelcomePageController : Controller
    {
        // GET: /<controller>/
        public IActionResult Index()
        {
            //Инициализация сервисов при автозагрузке
            return View();
        }
    }
}
