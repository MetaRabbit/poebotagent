﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.AspNet.Mvc;
using Microsoft.Extensions.FileSystemGlobbing;
using PoeBotAgent.Core;
using PoeBotAgent.Extends;
using PoeBotAgent.Service;
using PoeBotAgent.Utils;
using Serilog;

namespace PoeBotAgent.Controllers
{
    /// <summary>
    /// Контроллер отвечает за плагины к боту
    /// </summary>
    [Route("api/[controller]")]
    public class PluginController : Controller
    {
        Configuration _configuration;
        PluginControl _pluginControl;

        public PluginController(Configuration configuration, PluginControl pluginControl)
        {
            _configuration = configuration;
            _pluginControl = pluginControl;
        }

        /// <summary>
        /// Взятие списка плагинов
        /// </summary>
        /// <returns></returns>
        // GET: api/Plugin
        [HttpGet]
        public IEnumerable<string> Get()
        {

            return _pluginControl.GetListPlugins();
          
        }
        //Взятие настроек конкретного плагина
        // GET api/Plugin/AutoLogin
        [HttpGet("{name}")]
        public JsonResult Get(string name)
        {
            return Json(_pluginControl.GetSettingPluginByName(name));
        }


        /// <summary>
        /// Изменение настроек конкретного плагина
        /// </summary>
        /// <param name="name"></param>
        /// <param name="items"></param>
        [HttpPost("{name}")]
        public void Post(string name, [FromBody]List<string> items)
        {
            if (items == null)
            {
                Response.BadRequest();
                return;

            }
            _pluginControl.ChangeSettings(name, items);
        }


        /// <summary>
        /// Закачать плагин в папку с ботом
        /// </summary>
        /// <param name="items"></param>
        // POST api/plugin/AutoLogin
        [HttpPut]
        public void Put()
        {
           

        }

        /// <summary>
        /// Удаление плагина
        /// </summary>
        /// <param name="id"></param>
        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
