﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Http;


namespace PoeBotAgent.Extends
{
    public static class ResponseExtensions
    {
        public static void BadRequest(this HttpResponse result)
        {
            result.StatusCode = 400;
        }
        public static void NotFoundRequest(this HttpResponse result)
        {
            result.StatusCode = 404;
        }
        public static void ServerErrorRequest(this HttpResponse result)
        {
            result.StatusCode = 500;
        }
    }
}
