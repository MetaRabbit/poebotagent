﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PoeBotAgent.Core.Exilebuddy;
using PoeBotAgent.Core.Interface;
using PoeBotAgent.Enum;
using PoeBotAgent.Models;
using PoeBotAgent.Service;
using PoeBotAgent.Utils;
using Serilog;

namespace PoeBotAgent.Core
{
    public class BotConfiguration
    {
        public string ArgsRun { get; private set; }
        public bool IsNeedUpdate { get; private set; }
        public JObject AppBotSettings { get; set; }

        IBotSettingsChanger _botSettingsChanger;
        Configuration _configuration;

        public IBotSettingsChanger BotSettingsChanger
        {
            get
            {
                if (_configuration.Settings.Type == TypeAgent.Exilebuddy)
                {
                    if (_botSettingsChanger != null && _botSettingsChanger.GetType() == typeof(ExilebuddySettingsChanger))
                    {
                        return _botSettingsChanger;
                    }
                    _botSettingsChanger = new ExilebuddySettingsChanger(_configuration);
                }
             
              return _botSettingsChanger;         
            }
        }

        public BotConfiguration(Configuration configuration)
        {
            _configuration = configuration;
            //Загрузка
            ArgsRun = JsonConvert.DeserializeObject<string>(File.ReadAllText("botsettings.json"));

            if (_configuration.Settings.AbsPathBotDirectory == null ||
                _configuration.Settings.PathBotSettingsFile == null)
            {
                Log.Error("Не найден один из путей: Абсолютный путь к директории бота или относительный путь к дополнительному файлу настроек бота.");
                AppBotSettings = null;
            }
            else
            {
                if (!File.Exists(Path.Combine(_configuration.Settings.AbsPathBotDirectory,
           _configuration.Settings.PathBotSettingsFile)))
                {
                    Log.Error("Не найден дополнительный файл настроек. Проверьте правильность указания абсолютного пути к каталогу бота. " + Path.Combine(_configuration.Settings.AbsPathBotDirectory,
                    _configuration.Settings.PathBotSettingsFile));
                }

                AppBotSettings =
               JObject.Parse(
                   File.ReadAllText(Path.Combine(_configuration.Settings.AbsPathBotDirectory,
                       _configuration.Settings.PathBotSettingsFile)));
            }            
        }

        /// <summary>
        /// Изменяет по возможности настройки бота.
        /// </summary>
        /// <param name="botSettings"></param>
        /// <param name="args"></param>
        /// <param name="isNeedUpdate"></param>
        /// <returns>Возвращает флаг необходимости перезагрузки</returns>
        public void Change(BotSettings botSettings, string args,bool isNeedUpdate)
        {
            //Применяем основные настройки

            IsNeedUpdate = isNeedUpdate;
            //Заменяем настройки основного бота, если изначально отсутствуют, либо другие
            if (IsReplace(BotSettingsChanger.MainBot, botSettings.MainBot) || IsReplace(args, ArgsRun) || IsReplace(botSettings.Key, BotSettingsChanger.AuthKeys) || IsReplace(botSettings.MainRoutine, BotSettingsChanger.MainRoutine))
            {
                if (botSettings.MainBot != null)
                {
                    BotSettingsChanger.ChangeMainBot(botSettings.MainBot);
                }
                if (!string.IsNullOrEmpty(args))
                {
                    ArgsRun = args;
                }
                if (botSettings.MainRoutine != null)
                {
                    BotSettingsChanger.ChangeMainRoutine(botSettings.MainRoutine);
                }
                if (botSettings.Key != null)
                {
                    BotSettingsChanger.ChangeKey(botSettings.Key);
                }
                //Применяем основные настройки в боте
                BotSettingsChanger.Save();
            }

            //Применяем дополнительные настройки
            if (AppBotSettings != null && botSettings.AdvanceItems != null)
            {
                ApplyAdvanceSettings(botSettings.AdvanceItems);
                Save();

            }
            //Сохранение настроек
            SaveBotSetting();
        }

        public void ApplyAdvanceSettings(List<string> settings)
        {
            foreach (var advanceItem in settings)
            {
                var key = advanceItem.Split(':')[0];
                var value = advanceItem.Split(':')[1];
                JToken token;
                if (AppBotSettings.TryGetValue(key, out token))
                {
                    int result;
                    if (Int32.TryParse(value, out result))
                    {
                        AppBotSettings[key] = result;
                        continue;                        
                    }
                    bool bresult;
                    if (bool.TryParse(value, out bresult))
                    {
                        AppBotSettings[key] = bresult;
                        continue;
                    }
                    AppBotSettings[key] = value;                                                            
                }               
            }
        }

        private bool IsReplace(string oldValue, string newValue)
        {
            if (!string.IsNullOrEmpty(newValue) && string.IsNullOrEmpty(oldValue) || !string.IsNullOrEmpty(newValue) && !newValue.Equals(oldValue))
            {
                return true;
            }
            return false;
        }

        private void SaveBotSetting()
        {
            File.WriteAllText("botsettings.json", JsonConvert.SerializeObject(ArgsRun));
        }

        private void Save()
        {

           if (AppBotSettings != null && _configuration.Settings.AbsPathBotDirectory != null && _configuration.Settings.PathBotSettingsFile != null)
           {
                File.WriteAllText(Path.Combine(_configuration.Settings.AbsPathBotDirectory, _configuration.Settings.PathBotSettingsFile), AppBotSettings.ToString());
           }
                     
        }
    }
}
