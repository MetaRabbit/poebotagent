﻿using System.IO;
using Newtonsoft.Json;
using PoeBotAgent.Models;

namespace PoeBotAgent.Core
{
    public class Configuration
    {
        private readonly string _path;
        public Settings Settings { get; private set; }


        public Configuration()
        {
            _path = "settings.json";
            Settings = JsonConvert.DeserializeObject<Settings>(File.ReadAllText(_path));
        }

        public Settings GetSettings()
        {
            return Settings;
        } 

        public void Save()
        {
            File.WriteAllText(_path, JsonConvert.SerializeObject(Settings));
        }

        public void AddOrUpdate(Settings settings)
        {
            //Проверка на обязательные разделы
            Settings = settings;
            Save();
        }

        
  
    }
}
