﻿using System;
using System.IO;
using System.Net.Http;
using System.Text.RegularExpressions;
using Ionic.Zip;
using PoeBotAgent.Core.Exilebuddy;
using PoeBotAgent.Core.Interface;
using PoeBotAgent.Enum;
using PoeBotAgent.Utils;
using PoeBotAgent.Utils.System;
using Serilog;

namespace PoeBotAgent.Core
{
    public class UpdaterBot
    {
        readonly Configuration _configuration;
        public static readonly string BotTempPath = "BotCopy";
        public bool Updating { get; private set; }
        ILogger _logger;

        private IPatcher _patcher;
        private static string _lastVersion;
        private static string _botArchiveName = "bot.zip";

        public IPatcher Patcher
        {
            get
            {
                if (_configuration.Settings.Type == TypeAgent.Exilebuddy)
                {
                    if (_patcher != null && _patcher.GetType() == typeof (ExilebuddyPatcher))
                    {
                        return _patcher;
                    }
                    _patcher = new ExilebuddyPatcher(_configuration);
                }

                return _patcher;
            }
        }

        public UpdaterBot(Configuration configuration)
        {
            _configuration = configuration;
            _logger = Log.ForContext<UpdaterBot>();
        }

        public bool CheckUpdate()
        {
            if (!Ready())
            {
                return false;
            }
            using (HttpClient client = new HttpClient())
            {
                var document = client.GetStringAsync(_configuration.Settings.UrlUpdatePage).Result;

                string pattern = $@"{_configuration.Settings.BotName}:\s([\d.]*)\sr([\d.]*)\sBuild\s([\d]*)";
                var matches = Regex.Match(document, pattern);
                if (matches.Groups.Count == 0)
                {
                    Log.Error("На сайте не найдены сведения о версии, либо сведения неккоректны");
                    return false;
                }

                var stringVersion = $"{matches.Groups[1]}.{matches.Groups[2]}.{matches.Groups[3]}";
                _lastVersion = stringVersion;
                return !stringVersion.Equals(_configuration.Settings.Version);
            }

        }

        public bool Update()
        {
            if (!Ready())
            {
                return false;
            }
            //Ставим флаг обновления в True
            Updating = true;
            //Скачивание архива с новым ботом
            DownloadArchiveBot();
            //Разархивирование архива во временную папку
            ReCreateTempDirectoryBot();
            ExtractToTempDirectory();
            //Копирование папки плагинов
            //CopyPluginsDirectory();
            //Копирование папки настроек плагинов
            CopyPluginSettingsDirectory();
            //Применение конкретных патчей конкретного бота
            Patcher.Patch();
            //Копирование файлов из временной папки в конечную
            CopyTempToDestination();
            //Удаление временных папок и файлов
            DeleteTempData();
            //Смена в настройках версии игры
            UpdateConfiguration();
            //Снимаем флаг обновления
            Updating = true;
            return true;
        }

        private void CopyTempToDestination()
        {
            FileHelper.CopyDir(BotTempPath, _configuration.Settings.AbsPathBotDirectory);
        }

        private void UpdateConfiguration()
        {
            _configuration.Settings.Version = _lastVersion;
            _configuration.Save();
        }

        private void CopyPluginsDirectory()
        {
            FileHelper.CopyDir(
                Path.Combine(_configuration.Settings.AbsPathBotDirectory, _configuration.Settings.PathPlugins),
                Path.Combine(BotTempPath, _configuration.Settings.PathPlugins));
        }

        private void CopyPluginSettingsDirectory()
        {
            FileHelper.CopyDir(
                Path.Combine(_configuration.Settings.AbsPathBotDirectory, _configuration.Settings.PathToSettingsPlugins),
                Path.Combine(BotTempPath, _configuration.Settings.PathToSettingsPlugins));
        }

        private void DeleteTempData()
        {
            Directory.Delete(BotTempPath, true);
            File.Delete(_botArchiveName);
        }

        private void ReCreateTempDirectoryBot()
        {
            if (Directory.Exists(BotTempPath))
            {
                Directory.Delete(BotTempPath, true);
            }
            Directory.CreateDirectory(BotTempPath);
        }

        private void ExtractToTempDirectory()
        {
            using (var stream = File.OpenRead(_botArchiveName))
            {
                var zipFile = ZipFile.Read(stream);
                zipFile.ExtractAll(BotTempPath);
            }
        }

        private void DownloadArchiveBot()
        {
            try
            {
                using (var httpClient = new HttpClient())
                {
                    using (
                        var request = new HttpRequestMessage(HttpMethod.Get,
                            _configuration.Settings.UrlUpdatePage + _configuration.Settings.PrefixDownloadLing +
                            _configuration.Settings.BotName))
                    {
                        using (
                            Stream contentStream = httpClient.SendAsync(request).Result.Content.ReadAsStreamAsync().Result,
                                stream = new FileStream("bot.zip", FileMode.Create, FileAccess.Write, FileShare.None))
                        {
                            contentStream.CopyToAsync(stream);
                        }
                    }
                }
            }
            catch (Exception e)
            {
               _logger.Error("Ошибка при скачивании новой версии: "+ e.Message); 
            }
            
        }


        public bool Ready()
        {

            if (string.IsNullOrEmpty(_configuration.Settings.UrlUpdatePage))
            {
                _logger.Error("На задан URL страницы с версией бота");
                return false;
            }
            if (string.IsNullOrEmpty(_configuration.Settings.Version))
            {
                _logger.Error("На задан параметр версии бота в настройках");
                return false;
            }
            if (string.IsNullOrEmpty(_configuration.Settings.PrefixDownloadLing) ||          
              string.IsNullOrEmpty(_configuration.Settings.BotName))
            {
                _logger.Error(
                    "На задан URL страницы с префиксом для скачивания или страницы версии бота или имени бота: PrefixDownloadLing, BotName");
                return false;
            }
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    var document = client.GetStringAsync(_configuration.Settings.UrlUpdatePage).Result;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

    }
}
