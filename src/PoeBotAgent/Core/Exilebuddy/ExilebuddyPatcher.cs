﻿using System.IO;
using PoeBotAgent.Core.Interface;

namespace PoeBotAgent.Core.Exilebuddy
{
    public class ExilebuddyPatcher : IPatcher
    {
        Configuration _configuration;

        public ExilebuddyPatcher(Configuration configuration)
        {
            _configuration = configuration;
        }

        public void Patch()
        {
            //Копирование Gui

            //Копирование глобальных настроек
            Directory.CreateDirectory(Path.Combine(UpdaterBot.BotTempPath, @"Settings\Global"));
            File.Copy(Path.Combine(_configuration.Settings.AbsPathBotDirectory, @"Settings\Global\GlobalSettings.json"), Path.Combine(UpdaterBot.BotTempPath, @"Settings\Global\GlobalSettings.json"));
            //Переименовываем файл
            File.Copy(Path.Combine(UpdaterBot.BotTempPath, @"Exilebuddy.exe"), Path.Combine(UpdaterBot.BotTempPath, _configuration.Settings.NameBotExeFile + ".exe"));
        }
    }
}
