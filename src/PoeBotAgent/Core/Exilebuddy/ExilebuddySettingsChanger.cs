﻿using System;
using System.IO;
using Newtonsoft.Json.Linq;
using PoeBotAgent.Core.Interface;
using Serilog;

namespace PoeBotAgent.Core.Exilebuddy
{
    public class ExilebuddySettingsChanger : IBotSettingsChanger
    {
        JObject _guiSettings;
        JObject _globalSettings;
        Configuration _configuration;

        public ExilebuddySettingsChanger(Configuration configuration)
        {
            _configuration = configuration;
            if (configuration.Settings.AbsPathBotDirectory == null)
            {
                Log.Error("Не указан абсолютный путь к директории бота");
                _guiSettings = null;
                _globalSettings = null;

            }
            else
            {

                if (!File.Exists(Path.Combine(configuration.Settings.AbsPathBotDirectory, @"Settings\Default\Gui.json")))
                {
                    Log.Error("Не найден файл настроек графического интерфейса. Проверьте правильность указания абсолютного пути к каталогу бота. " + Path.Combine(configuration.Settings.AbsPathBotDirectory, @"Settings\Default\Gui.json"));
                    _guiSettings = null;
                }

                _guiSettings = JObject.Parse(File.ReadAllText(Path.Combine(configuration.Settings.AbsPathBotDirectory, @"Settings\Default\Gui.json")));

                if (!File.Exists(Path.Combine(configuration.Settings.AbsPathBotDirectory, @"Settings\Global\GlobalSettings.json")))
                {
                    Log.Error("Не найден глобальный файл настроек бота. Проверьте правильность указания абсолютного пути к каталогу бота. " + Path.Combine(configuration.Settings.AbsPathBotDirectory, @"Settings\Global\GlobalSettings.json"));
                    _globalSettings = null;
                }

                _globalSettings = JObject.Parse(File.ReadAllText(Path.Combine(configuration.Settings.AbsPathBotDirectory, @"Settings\Global\GlobalSettings.json")));
            }           
        }

        public void ChangeMainBot(string nameBot)
        {
            if (_guiSettings == null)
            {
                return;
            }
            if (nameBot != _guiSettings["LastBot"].Value<string>())
            {
                _guiSettings["LastBot"] = nameBot;
            }
           
        }

        public void ChangeKey(string key)
        {
            if (_globalSettings == null)
            {
                return;
            }

            if (key != _globalSettings["AuthKeys"].Value<string>())
            {
                _globalSettings["AuthKeys"] = key;
            }
        }

        public void ChangeMainRoutine(string nameRoutine)
        {
            if (_guiSettings == null)
            {
                return;
            }

            if (nameRoutine != _guiSettings["LastRoutine"].Value<string>())
            {
                _guiSettings["LastRoutine"] = nameRoutine;
            }
            _guiSettings["LastRoutine"] = nameRoutine;
        }

        public string MainBot => _guiSettings == null ? "Файла с настройками не существует. Проверьте путь к папке бота." : _guiSettings["LastBot"].Value<string>();
        public string MainRoutine => _guiSettings == null ? "Файла с настройками не существует. Проверьте путь к папке бота." : _guiSettings["LastRoutine"].Value<string>();
        public string AuthKeys => _globalSettings == null ? "Файла с настройками не существует. Проверьте путь к папке бота." : _globalSettings["AuthKeys"].Value<string>();

        public void Save()
        {
            try
            {
                if (_guiSettings != null)
                {
                    File.WriteAllText(Path.Combine(_configuration.Settings.AbsPathBotDirectory, @"Settings\Default\Gui.json"), _guiSettings.ToString());
                }

                if (_globalSettings != null)
                {
                    File.WriteAllText(Path.Combine(_configuration.Settings.AbsPathBotDirectory, @"Settings\Global\GlobalSettings.json"), _globalSettings.ToString());
                }
                
            }
            catch (Exception e)
            {
                Log.Error(e.Message);
                Log.Error(e.Message);
            }
           
        }
    }
}
