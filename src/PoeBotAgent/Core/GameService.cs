﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using Newtonsoft.Json;
using PoeBotAgent.Enum;
using PoeBotAgent.Utils.System;
using Serilog;

namespace PoeBotAgent.Core
{
    public class GameService
    {
        Configuration _configuration;

        public bool IsUpdating => WindowsControl.IsExistsWindows(_configuration.Settings.UpdateWindowsNameList);

        public string[] Settings
        {
            get
            {

                if (_configuration.Settings.AbsPathGameSettings == null)
                {
                    Log.Error("Не задан следующий путь: AbsPathGameSettings");
                    return new []{ "Не задан следующий путь: AbsPathGameSettings" };
                }
                if (!File.Exists(_configuration.Settings.AbsPathGameSettings))
                {
                    Log.Error("Не найден файл настроек игры");
                    return new[] { "Не найден файл настроек игры" }; ;
                }
                try
                {
                    return File.ReadAllLines(_configuration.Settings.AbsPathGameSettings);
                }
                catch (Exception)
                {
                    Log.Error("Не корректый формат пути: AbsPathGameSettings");
                    return new[] { "Не корректый формат пути: AbsPathGameSettings" }; ;
                }
                
            }
        }

        static object _locker = new object();

        public GameService(Configuration configuration)
        {
            _configuration = configuration;
        }

        public bool Running => ProcessControl.IsExecute(_configuration.Settings.NameGameExeFile);

        public void UseDefaultSettings(bool used)
        {
            if (!used) return;
            if (string.IsNullOrEmpty(_configuration.Settings.NameGame))
            {
                Log.Error("Не задано имя игры в настройках: NameGame");
                return;
            }
            if (!File.Exists("gamesettings\\" + _configuration.Settings.NameGame + "_min"))
            {
                Log.Error("Не найдены минимальные настройки графики для игры" + "NameGame");
                return;
            }
            try
            {
                var settings =
                    JsonConvert.DeserializeObject<List<string>>(
                        File.ReadAllText("gamesettings\\" + _configuration.Settings.NameGame + "_min"));
                ChangeState(settings);
            }
            catch (Exception)
            {
                Log.Error("Не верный формат имени игры в настройках: " + _configuration.Settings.NameGame);
            }
        }

        public void ChangeState(List<string> settings)
        {
            if (settings == null) return;
            if (_configuration.Settings.AbsPathGameSettings == null)
            {
                Log.Error("Не задан следующий путь: AbsPathGameSettings");
                return;
            }
            if (!File.Exists(_configuration.Settings.AbsPathGameSettings))
            {
                Log.Error("Не найден файл настроек игры");
                return;
            }
                Monitor.Enter(_locker);
            try
            {
                //Загрузка текущих настроек
                var currentSettings = File.ReadAllLines(_configuration.Settings.AbsPathGameSettings);
                //Создание пустого массива данных
                var newSettings = new List<string>();
                foreach (var currentSetting in currentSettings)
                {
                    var added = false;
                    foreach (
                        var setting in
                            settings.Where(setting => setting.Split('=')[0].Equals(currentSetting.Split('=')[0])))
                    {
                        //Добавление измененного параметра
                        newSettings.Add(currentSetting.Replace(currentSetting.Split('=')[1], setting.Split('=')[1]));
                        added = true;
                    }
                    if (!added)
                    {
                        //Добавление неизменившихся параметров
                        newSettings.Add(currentSetting);
                    }

                }
                //Запись файла настроек
                File.WriteAllLines(_configuration.Settings.AbsPathGameSettings, newSettings);
            }
            catch (Exception e)
            {
                Log.Error("Не верный формат пути к файлу настроек игры");
                Log.Error(e.Message);

            }
            finally
            {
                Monitor.Exit(_locker);
            }
        }

        public bool TrySwitchRunning(Running running)
        {
            lock (_locker)
            {
                if (string.IsNullOrEmpty(_configuration.Settings.AbsPathGameDirectory) || string.IsNullOrEmpty(_configuration.Settings.NameGameExeFile))
                {
                    Log.Error("Не задан один или оба пути: AbsPathBotDirectory и NameBotExeFile");
                    return false;
                }

                if (IsUpdating)
                {
                    return false;
                }
                //Не запускать, если игра уже запущена
                if (running == Enum.Running.Enable && Running) return true;
                //Не выключать, если игра уже выключена 
                if (running == Enum.Running.Disable && !Running) return true;
                bool neenWait = ProcessControl.SwitchRunning(running, _configuration.Settings.AbsPathGameDirectory, _configuration.Settings.NameGameExeFile);

                //Дополнительное ожидание загрузки необходимого окна
                if (neenWait)
                {
                    Thread.Sleep(TimeSpan.FromMinutes(2));
                }

                //Проверить не запустился ли апдейтер у игры
                if (IsUpdating)
                {
                    return false;
                }
                return true;
            }
          
        }
    }
}
