﻿
using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PoeBotAgent.Core.Interface;
using PoeBotAgent.Enum;
using PoeBotAgent.Models;
using PoeBotAgent.Service;
using PoeBotAgent.Utils;
using Serilog;
namespace PoeBotAgent.Core.Interface
{
    public interface IBotSettingsChanger
    {
        string MainBot { get; }
        string MainRoutine { get; }
        string AuthKeys { get; }

        void ChangeMainBot(string nameBot);
        void ChangeKey(string key);
        void ChangeMainRoutine(string nameRoutine);
        void Save();
    }
}
