﻿namespace PoeBotAgent.Core.Interface
{
    public interface IPatcher
    {
        void Patch();
    }
}
