﻿using System;
using System.IO;
using System.Threading;
using PoeBotAgent.Enum;
using PoeBotAgent.Models;
using PoeBotAgent.Service;
using PoeBotAgent.Utils.System;
using Serilog;

namespace PoeBotAgent.Core
{
    public class BotService
    {
        Configuration _configuration;
        public BotConfiguration BotConfiguration { get; }
        GameService _gameService;
        UpdaterBot _updateBotService;
        static object _locker = new object();

        public bool Running => ProcessControl.IsExecute(_configuration.Settings.NameBotExeFile);

        public BotService(Configuration configuration, BotConfiguration botConfiguration, GameService gameService, UpdaterBot updateBotService)
        {

            _configuration = configuration;
            BotConfiguration = botConfiguration;
            _gameService = gameService;
            _updateBotService = updateBotService;
        }


        public void ChangeState(BotState botState)
        {
            if (botState.BotSettings != null)
            {
               BotConfiguration.Change(botState.BotSettings, botState.StartArgs, botState.IsNeedUpdate);
            }

            if (botState.Running != null)
            {
                //Проверка запуска игры - запуск по необходимости
                if (!_gameService.TrySwitchRunning(Enum.Running.Enable))
                {
                    Log.Warning("Невозможно запустить бота без запущенного игрового процесса. Либо игра обновляется, либо не запускается.");

                    return;
                }


                TrySwitchRunning(botState.Running.Value);

            }
        }

        public bool TrySwitchRunning(Running running)
        {
            switch (running)
            {
                case Enum.Running.Enable:
                    {
                        if (!Running)
                        {
                            StartBot();
                            return true;
                        }
                        else
                        {
                            return false;
                        }

                    }
                case Enum.Running.Disable:
                    {
                        if (Running)
                        {
                            KillBot();
                        }
                        break;
                    }
                case Enum.Running.Reload:
                    {
                        TrySwitchRunning(Enum.Running.Disable);
                        TrySwitchRunning(Enum.Running.Enable);
                        break;
                    }
            }
            return false;


        }

        private void KillBot()
        {
            if (string.IsNullOrEmpty(_configuration.Settings.AbsPathBotDirectory) || string.IsNullOrEmpty(_configuration.Settings.NameBotExeFile))
            {
                Log.Error("Не задан один из следующих путей: AbsPathBotDirectory, NameBotExeFile");
                return;
            }         
            ProcessControl.Kill(_configuration.Settings.NameBotExeFile);
        }

        //Метод старта

        private void StartBot()
        {
            if (string.IsNullOrEmpty(_configuration.Settings.AbsPathBotDirectory) || string.IsNullOrEmpty(_configuration.Settings.NameBotExeFile))
            {
                Log.Error("Не задан один из следующих путей: AbsPathBotDirectory, NameBotExeFile");
                return;
            }

            if (!File.Exists(Path.Combine(_configuration.Settings.AbsPathBotDirectory, _configuration.Settings.NameBotExeFile+".exe")))
            {
                Log.Error("Исполняемый файл бота не найден по пути " + Path.Combine(_configuration.Settings.AbsPathBotDirectory, _configuration.Settings.NameBotExeFile));
                return;
            }
            Monitor.Enter(_locker);
            try
            {
             
                //Проверка готовки сервиса обновления
                if (!_updateBotService.Ready())
                {
                    return;
                }

                //Проверка апдейта и флага
                //Обновление при необходимости
                if (_updateBotService.CheckUpdate())
                {
                    _updateBotService.Update();
                }
                ProcessControl.Start(_configuration.Settings.AbsPathBotDirectory,_configuration.Settings.NameBotExeFile);
                //запуск бота, если предыдущие пункты выполнились успешно
            }
            catch (Exception e)
            {
               //Неизвестная ошибка

            }
            finally
            {
                Monitor.Exit(_locker);
            }
                 
        }

    }
}
