﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PoeBotAgent.Enum
{
    public enum TypeObject
    {
        Game,
        Bot,
        Plugins
    }

    public enum Running
    {
        Enable = 1,
        Disable = 0,
        Reload = 2
    }

    public enum TypeAgent
    {
        Exilebuddy
    }
}
