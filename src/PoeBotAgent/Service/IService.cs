﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PoeBotAgent.Service
{
    public interface IService
    {
        void Start();
        bool Ready();
        bool AutoStartAvailable { get; }
        bool IsRunning { get; }
    }
}
