﻿using System;
using System.Threading;
using PoeBotAgent.Core;
using PoeBotAgent.Enum;
using Serilog;

namespace PoeBotAgent.Service
{
    public class ProcessReloadService : IService
    {
        public string Status;
        Timer timer;
        Configuration _configuration;
        private static int _sleepTime = 60000;
        ILogger _log;
        int _nAttempt;
        BotService _botService;
        GameService _gameService;
        UpdaterBot _updateBotService;


        public ProcessReloadService(Configuration configuration, BotService botService, GameService gameService)
        {
            _configuration = configuration;
            _botService = botService;
            _gameService = gameService;
            _log = Log.ForContext<ProcessReloadService>();
        }

        public void Start()
        {
            //Проверяем состояние процессов каждую минуту
            timer = new Timer(state =>
            {
                //Проверка на наличие необходимых параметров
                if (string.IsNullOrEmpty(_configuration.Settings.NameBotExeFile) || string.IsNullOrEmpty(_configuration.Settings.NameGameExeFile) || string.IsNullOrEmpty(_configuration.Settings.AbsPathGameDirectory) || string.IsNullOrEmpty(_configuration.Settings.NameGameExeFile))
                {
                    _log.Warning("Невозможна работа сервиса перезагрузку, т.к. не задан или задан неверно один из иледующих параметров: NameBotExeFile, NameGameExeFile, AbsPathGameDirectory, NameGameExeFile");
                    
                }
                else
                {
                    //Проверка запуска игры - запуск по необходимости
                    if (!_gameService.TrySwitchRunning(Running.Enable))
                    {
                        _log.Warning("Невозможно запустить бота без запущенного игрового процесса. Либо игра обновляется, либо не запускается.");

                        return;
                    }
                    //Попытка запустить бота.
                    if (!_botService.TrySwitchRunning(Running.Enable) && !_updateBotService.Updating)
                    {
                        //Если бота не запустить, и он не обновляется - проверить его работу
                        //Если лог молчит  
                        //Если попыток запуска больше 3,
                        if (_nAttempt != 3)
                        {
                            //Убиваем процесс игры
                            //Сбрасываем попытки
                        }
                        //Иначе
                        //Перезапуск бота
                        //Увеличение попытки
                    }
                }

            }, null, TimeSpan.FromMilliseconds(1000), TimeSpan.FromMinutes(1));
        }

        public bool Ready()
        {
            throw new NotImplementedException();
        } 

        public bool AutoStartAvailable { get; }
        public bool IsRunning { get; }
    }

}
